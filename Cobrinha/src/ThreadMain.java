import java.awt.event.*;
import javax.swing.*;

class ThreadMain implements KeyListener{

	@Override
	public void keyPressed(KeyEvent e) {
		
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_W) {
			System.out.println("W" + key);
		}
		
		if(key == KeyEvent.VK_A) {
			System.out.println("A" + key);
		}
		
		if(key == KeyEvent.VK_S) {
			System.out.println("S" + key);
		}
		
		if(key == KeyEvent.VK_D) {
			System.out.println("D" + key);
		}
		
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		//System.out.println("Despertou");
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	public static void main(String[] args) {
		
		JFrame jf = new JFrame("key event");
		jf.setSize(400,400);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		jf.addKeyListener(new ThreadMain());
		jf.setVisible(true);
	}
}
