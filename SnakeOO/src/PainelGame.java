import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class PainelGame extends JPanel implements Runnable, KeyListener {
	
	private static final long serialVersionUID = 1L;

	private static final int LARGURA = 500, ALTURA = 500;
	
	private Thread thread;
	
	private boolean running;
	
	private CorpoSnake b;
	private ArrayList<CorpoSnake> snake;
	
	private Comida comida;
	private ArrayList<Comida> comidas;
	
	private Random r;
	
	private int x = 10;
	private int y = 10;
	private int tamanho = 20;
		
	private int ticks = 0;
	
	private boolean direita = false;
	private boolean esquerda = false;
	private boolean cima = false;
	private boolean baixo = true;
	
	public PainelGame() {
		setFocusable(true);
		
		setPreferredSize(new Dimension(LARGURA, ALTURA));
		addKeyListener(this);
		
		snake = new ArrayList<CorpoSnake>();
		comidas = new ArrayList<Comida>();
		
		r = new Random();
		
		start();
	}
	
	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void tick() {
		if(snake.size() == 0) {
			b = new CorpoSnake(x, y, 10);
			snake.add(b);
		}
		
		ticks++;
		
		if(ticks > 250000) {
			if(direita) x++;
			
			if(esquerda) x--;
			
			if(cima) y--;
			
			if(baixo) y++;
			
			ticks = 0;
			
			b = new CorpoSnake(x, y, 10);
			snake.add(b);
			
			if(snake.size() > tamanho) {
				snake.remove(0);
			}
		}
		
		if(comidas.size() == 0) {
			int x = r.nextInt(49);
			int y = r.nextInt(49);
			
			comida = new Comida(x, y, 10);
			comidas.add(comida);
		}
		
		for(int i = 0; i < comidas.size(); i++) {
			if(x == comidas.get(i).getX() && y == comidas.get(i).getY()) {
				tamanho++;
				comidas.remove(i);
				i++;
			}
		}
		
		//colisao da propria cobra
		for(int i = 0; i < snake.size(); i++) {
			if(x == snake.get(i).getX() && y == snake.get(i).getY()) {
				if(i != snake.size() - 1) {
					System.out.println("Game Over");
					stop();
				}
			}
		}
		//colisao de borda
		if(x < 0 || x > 49 || y < 0 || y > 49) {
			System.out.println("Game Over");
			stop();
		}
	}
	
	public void paint(Graphics g) {
		
		g.clearRect(0, 0, LARGURA, ALTURA);
		
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, LARGURA, ALTURA);
		
		for(int i = 0; i < LARGURA/10; i++) {
			g.drawLine(i * 10, 0, i * 10, ALTURA);
		}
		
		for(int i = 0; i < ALTURA/10; i++) {
			g.drawLine(0, i * 10, ALTURA, i * 10);
		}
		
		for(int i = 0; i < snake.size(); i++) {
			snake.get(i).draw(g);
		}
		
		for(int i = 0; i < comidas.size(); i++) {
			comidas.get(i).draw(g);
		}
	}

	@Override
	public void run() {
		while(running) {
			tick();
			repaint();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_RIGHT && !esquerda) {
			direita = true;
			cima = false;
			baixo = false;
		}
		
		if(key == KeyEvent.VK_LEFT && !direita) {
			esquerda = true;
			cima = false;
			baixo = false;
		}
		
		if(key == KeyEvent.VK_UP && !baixo) {
			direita = false;
			cima = true;
			esquerda = false;
		}
		
		if(key == KeyEvent.VK_DOWN && !cima) {
			direita = false;
			esquerda = false;
			baixo = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
